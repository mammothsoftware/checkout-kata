insert into PRODUCT
(SKU, PRODUCT_NAME, UNIT_PRICE)
VALUES('G', 'Grapes', 1.00);

insert into DISCOUNT
(UUID, AMOUNT, SPECIAL_PRICE, SKU)
VALUES('test_1', 3, 2.00, 'G');