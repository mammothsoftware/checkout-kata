drop table if exists DISCOUNT;
drop table if exists PRODUCT;

create table PRODUCT
(
    SKU          char primary key not null unique,
	PRODUCT_NAME varchar(50) not null,
	UNIT_PRICE   numeric not null
);

create table DISCOUNT
(
    UUID            varchar(50) primary key not null,
    AMOUNT          integer not null,
	SPECIAL_PRICE   numeric not null,
    SKU             varchar(50) not null references PRODUCT (SKU)
);
