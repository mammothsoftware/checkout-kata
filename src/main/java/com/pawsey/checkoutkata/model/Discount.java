package com.pawsey.checkoutkata.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Data
@Entity
public class Discount {

    @Id
    private String uuid;

    private int amount;
    private float specialPrice;

    private Character sku;

    public Discount(){}

    public Discount(String uuid, int amount, float specialPrice, Character sku) {
        this.uuid = uuid;
        this.amount = amount;
        this.specialPrice = specialPrice;
        this.sku = sku;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public float getSpecialPrice() {
        return specialPrice;
    }

    public void setSpecialPrice(float specialPrice) {
        this.specialPrice = specialPrice;
    }

    public Character getSku() {
        return sku;
    }

    public void setSku(Character sku) {
        this.sku = sku;
    }
}
