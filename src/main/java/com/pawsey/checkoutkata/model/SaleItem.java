package com.pawsey.checkoutkata.model;

public class SaleItem {
    private Character sku;

    public SaleItem(){}

    public SaleItem(Character sku) {
        this.sku = sku;
    }

    public Character getSku() {
        return sku;
    }
}
