package com.pawsey.checkoutkata.controller;

import com.pawsey.checkoutkata.model.SaleItem;
import javassist.NotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.http.MediaType.ALL_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RequestMapping(
        value = "/sale")
public interface SaleController {
    /**
     * Takes a sale request containing a list of product {@link com.pawsey.checkoutkata.model.SaleItem#sku sku} values, and returns a transaction value for the sale.
     *
     * @param requestJson A request containing a list of product SKU values as a JSON payload of a HTTP request.
     * @return The total cost for the sale within the JSON payload of the HTTP response.
     */
    @RequestMapping(
            method = POST,
            consumes = ALL_VALUE
    )
    ResponseEntity<String> post(
            @RequestBody SaleItem[] requestJson) throws NotFoundException;

}