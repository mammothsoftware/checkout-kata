package com.pawsey.checkoutkata.controller;

import com.pawsey.checkoutkata.model.SaleItem;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SaleControllerImpl implements SaleController {

    @Autowired
    private com.pawsey.checkoutkata.service.SaleService SaleService;

    public SaleControllerImpl(com.pawsey.checkoutkata.service.SaleService saleService) {
        SaleService = saleService;
    }

    @Override
    public ResponseEntity<String> post(SaleItem[] requestSaleJson) {

        Float totalCost = null;
        try {
            totalCost = SaleService.makeSale(requestSaleJson);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(serialise(
                            totalCost));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    private String serialise(float totalCost) {
        return "{\"totalCost\": " + totalCost + "}";
    }

}