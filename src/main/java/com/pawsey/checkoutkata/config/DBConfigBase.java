package com.pawsey.checkoutkata.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

public abstract class DBConfigBase {
    private static final Logger logger = LoggerFactory.getLogger(DBConfig.class);

    protected DataSource getDataSource(String jdbcUrl, String dbUsername, String dbPassword, String jdbcDriverClassName, String profile) {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(jdbcDriverClassName);
        dataSource.setUrl(jdbcUrl);
        dataSource.setUsername(dbUsername);
        dataSource.setPassword(dbPassword);

        logProfile(jdbcUrl, dbUsername, dbPassword, jdbcDriverClassName, profile);

        return dataSource;
    }

    private void logProfile(String jdbcUrl, String dbUsername, String dbPassword, String jdbcDriverClassName, String profile) {
        String loggingMessage = "Using profile " + profile;

        if (profile.contains("test")) {
            loggingMessage = loggingMessage + " with the following database connection:" +
                    "\nDriver class" + jdbcDriverClassName +
                    "\njdbc URL: " + jdbcUrl +
                    "\nUser: " + dbUsername +
                    "\nPassword: " + dbPassword;

            logger.info(loggingMessage);
            System.out.println(loggingMessage);
        } else {
            logger.info(loggingMessage);
            System.out.println(loggingMessage);
        }
    }
}
