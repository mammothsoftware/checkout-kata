package com.pawsey.checkoutkata.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@EnableTransactionManagement
@Configuration
public class DBConfig extends DBConfigBase {

    @Bean
    @Profile("dev")
    public DataSource devDatasource() {
        String jdbcUrl = System.getenv("DATABASE_URL_JDBC");
        String dbUsername = System.getenv("DATABASE_USERNAME");
        String dbPassword = System.getenv("DATABASE_PASSWORD");
        String jdbcDriverClassName = "org.postgresql.Driver";

        return getDataSource(jdbcUrl, dbUsername, dbPassword, jdbcDriverClassName, "dev");
    }

    @Bean
    @Profile("test-local")
    public DataSource localDatasource() {

        String jdbcUrl = "jdbc:postgresql://localhost:5432/checkout";
        String dbUsername = "postgres";
        String dbPassword = "postgres";
        String jdbcDriverClassName = "org.postgresql.Driver";

        return getDataSource(jdbcUrl, dbUsername, dbPassword, jdbcDriverClassName, "test-local");
    }

    @Bean
    @Profile("test-integration")
    public DataSource integrationDatasource() {

        String jdbcUrl = "jdbc:postgresql://localhost:5432/checkout";
        String dbUsername = "postgres";
        String dbPassword = "postgres";
        String jdbcDriverClassName = "org.postgresql.Driver";

        return getDataSource(jdbcUrl, dbUsername, dbPassword, jdbcDriverClassName, "test-integration");
    }

    @Bean
    @Profile("test-remote")
    public DataSource remoteIntegrationDatasource() {

        String jdbcUrl = "jdbc:postgresql://localhost:5432/postgres";
        String dbUsername = "postgres";
        String dbPassword = "";
        String jdbcDriverClassName = "org.postgresql.Driver";

        return getDataSource(jdbcUrl, dbUsername, dbPassword, jdbcDriverClassName, "test-remote");
    }

    @Bean
    @Profile("test")
    public DataSource testDataSource() {
        String h2JdbcUrl = "jdbc:h2:~/test";
        String h2DbUsername = "sa";
        String h2DbPassword = "";
        String jdbcDriverClassName = "org.h2.Driver";

        return getDataSource(h2JdbcUrl, h2DbUsername, h2DbPassword, jdbcDriverClassName, "test");
    }

}
