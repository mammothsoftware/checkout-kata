package com.pawsey.checkoutkata.service;

import com.pawsey.checkoutkata.model.SaleItem;
import javassist.NotFoundException;

public interface SaleService {

    Float makeSale(SaleItem[] productSkuList) throws NotFoundException;
}