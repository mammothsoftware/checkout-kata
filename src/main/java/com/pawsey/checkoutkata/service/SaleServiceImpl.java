// TODO this is WIP

package com.pawsey.checkoutkata.service;

import com.pawsey.checkoutkata.model.Discount;
import com.pawsey.checkoutkata.model.Product;
import com.pawsey.checkoutkata.model.SaleItem;
import com.pawsey.checkoutkata.repository.DiscountRepository;
import com.pawsey.checkoutkata.repository.ProductRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SaleServiceImpl implements SaleService {

    private ProductRepository productRepository;
    private DiscountRepository discountRepository;

    @Autowired
    public SaleServiceImpl(ProductRepository productRepository, DiscountRepository discountRepository) {
        this.productRepository = productRepository;
        this.discountRepository = discountRepository;
    }

    public Float makeSale(SaleItem[] saleItems) throws NotFoundException {

        Float cost = 0f;

        // Null checks
        if (saleItems != null) {

            ArrayList<SaleItem> productSkuList = new ArrayList<>();
            for (SaleItem saleItem : saleItems) {
                productSkuList.add(saleItem);
            }

            // Loop through SaleItem array
            for (int i = 0; i <= productSkuList.size() - 1; i++) {

                // search for matching Discounts
                Character sku = productSkuList.get(i).getSku();
                List<Discount> discounts = discountRepository.findBySku(sku);

                if (discounts != null && discounts.size() > 0) {
                    // if discounts found, try and find discount with amount
                    // that matches number of matching skus
                    Discount theDiscount = new Discount();
                    for (Discount discount : discounts) {
                        if (discount.getAmount() > theDiscount.getAmount()) {
                            theDiscount = discount;
                            cost = discount.getSpecialPrice() + ((productSkuList.size() - discount.getAmount()) * productRepository.findBySku(sku).getUnitPrice());

                        }
                    }

                    if (productSkuList.size() != theDiscount.getAmount()) {
                        // if no amount to sku-amount match found, select discount with greatest amount
                        for (Discount discount : discounts) {
                            if (discount.getAmount() >= productSkuList.size()) {
                                theDiscount = discount;
                                cost = discount.getSpecialPrice() + ((productSkuList.size() - discount.getAmount()) * productRepository.findBySku(sku).getUnitPrice());
                            }
                        }
                    } else {
                        // add Discount#special_price to cost
                        cost = theDiscount.getSpecialPrice();
                    }
                    // Remove used SKUs from list
                    for (int k = 0; k <= productSkuList.size() - 1; k++) {
                        if (productSkuList.get(k).getSku() == theDiscount.getSku()) {
                            productSkuList.remove(k);
                        }
                    }


                } else {
                    // If discount not found, find product with sku, and sku-array-length * Product#unitPrice
                    int matchNumber = 0;
                    for (SaleItem saleItem : productSkuList) {
                        if (saleItem.getSku() == sku) {
                            matchNumber++;
                        }
                    }
                    Product bySku = productRepository.findBySku(sku);

                    if (bySku == null) {
                        throw new NotFoundException("No products with SKU " + sku);
                    }

                    float value = bySku.getUnitPrice() * matchNumber;
                    cost = cost + value;
                }
            }
        }
        return cost;
    }
}