package com.pawsey.checkoutkata.repository;

import com.pawsey.checkoutkata.model.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface ProductRepository extends CrudRepository<Product, String> {

    @Query("SELECT pk FROM Product pk WHERE pk.sku=:sku")
    Product findBySku(@Param("sku") Character sku);
}