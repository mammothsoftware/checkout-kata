package com.pawsey.checkoutkata.repository;

import com.pawsey.checkoutkata.model.Discount;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DiscountRepository extends CrudRepository<Discount, String> {

    @Query("SELECT pk FROM Discount pk WHERE pk.sku=:sku")
    List<Discount> findBySku(@Param("sku") Character sku);
}
