package com.pawsey.checkoutkata.controller;

import com.pawsey.checkoutkata.model.SaleItem;
import com.pawsey.checkoutkata.service.SaleService;
import com.pawsey.checkoutkata.testutils.HttpTestUtil;
import javassist.NotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

public class SaleContollerTest {

    @Autowired
    SaleController sut;

    SaleService mockSaleService;
    private String ENDPOINT;

    private HttpTestUtil httpTestUtil;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws NotFoundException {

        httpTestUtil = new HttpTestUtil();

        mockSaleService = Mockito.mock(SaleService.class);
        when(mockSaleService.makeSale(any(SaleItem[].class))).thenReturn(1.3f);

        ENDPOINT = "/sale";

        sut = new SaleControllerImpl(mockSaleService);
//        ReflectionTestUtils.setField(sut, "saleService", mockSaleService);

        this.mockMvc = standaloneSetup(sut).build();
    }

    @Test
    public void testPost() throws Exception {

        String requestPayload = "[{\"sku\":\"A\"},{\"sku\":\"A\"},{\"sku\":\"A\"}]";
        httpTestUtil.makeHttpPost(ENDPOINT, requestPayload, mockMvc);

        verify(mockSaleService, times(1)).makeSale(any(SaleItem[].class));
    }

}