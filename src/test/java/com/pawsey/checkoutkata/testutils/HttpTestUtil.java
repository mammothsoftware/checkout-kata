package com.pawsey.checkoutkata.testutils;

import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

public class HttpTestUtil {

    public void makeHttpPost(String url, String requestJson, MockMvc mockMvc) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(url);
        request.contentType(MediaType.APPLICATION_JSON_UTF8);
        request.content(requestJson);

        mockMvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    public void makeHttpGet(String url, MockMvc mockMvc) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.get(url);

        mockMvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}