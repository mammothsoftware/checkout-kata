package com.pawsey.checkoutkata.testutils;

import org.junit.Ignore;

import static java.util.UUID.randomUUID;

@Ignore
public class TestData {

    public static final String DISCOUNT_1_UUID = randomUUID().toString();
    public static final Character PRODUCT_1_SKU = 'A';

    @Ignore
    public static class Product1 {
        public static final Character SKU = PRODUCT_1_SKU;
        public static final String PRODUCT_NAME = "Apples";
        public static final float UNIT_PRICE = 0.25f;
    }

    @Ignore
    public static class Discount1 {
        public static final String UUID = DISCOUNT_1_UUID;
        public static final int AMOUNT = 3;
        public static final float SPECIAL_PRICE = 1.3f;
        public static final Character SKU = PRODUCT_1_SKU;
    }
}
