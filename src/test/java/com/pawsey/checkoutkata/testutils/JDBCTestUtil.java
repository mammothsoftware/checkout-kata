package com.pawsey.checkoutkata.testutils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import static org.assertj.core.api.Fail.fail;

public class JDBCTestUtil {

    private static Connection conn = null;
    private static Statement stmt = null;

    public static void connectToDatabase(String jdbcDriverClass, String dbUrl, String userName, String password) {

        try {
            // Register JDBC driver
            Class.forName(jdbcDriverClass);

            // Open a connection
            conn = DriverManager.getConnection(dbUrl, userName, password);

        } catch (SQLException se) {
            fail("JDBC connection to database failed:" +
                    "\nURL: " + dbUrl +
                    "\nConsumer: " + userName +
                    "\nPassword: " + password
            );
        } catch (Exception e) {
            fail("Failed to register JDBC driver");
        }
    }

    public static void executeDatabaseUpdate(String sqlStatement) {

        try {
            stmt = conn.createStatement();
        } catch (SQLException e) {
            fail("Failed to get database connection");
        }

        try {
            stmt.executeUpdate(sqlStatement);
        } catch (SQLException e) {
            fail(e.getCause() + ": " + e.getLocalizedMessage() + ": " + e.getErrorCode() + ": " + e.getSQLState() + ": " + e.getNextException());
        } finally {
            try {
                stmt.close();
            } catch (SQLException e) {
                fail("Failed to close statement after attempting to create the statement" + e.getSQLState());
            }
        }

    }


    public static void closeDatabase() {
        try {
            stmt.close();
        } catch (SQLException e) {
            fail("Failed to close statement");
        }
        try {
            conn.close();
        } catch (SQLException e) {
            fail("Failed to close connection");
        }
    }
}