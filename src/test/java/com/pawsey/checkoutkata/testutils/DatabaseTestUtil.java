package com.pawsey.checkoutkata.testutils;

import com.pawsey.checkoutkata.model.Discount;
import com.pawsey.checkoutkata.model.Product;

import static org.assertj.core.api.Assertions.fail;

public class DatabaseTestUtil {

    private String jdbcDriverClassName;
    private String jdbcUrl;
    private String dbUsername;
    private String dbPassword;

    public DatabaseTestUtil() {
        jdbcDriverClassName = "org.postgresql.Driver";
        jdbcUrl = "jdbc:postgresql://localhost:5432/checkout";
        dbUsername = "postgres";
        dbPassword = "postgres";

        insertIntoDatabase();
    }

    public DatabaseTestUtil(String jdbcDriverClassName, String jdbcUrl, String dbUsername, String dbPassword) {
        this.jdbcDriverClassName = jdbcDriverClassName;
        this.jdbcUrl = jdbcUrl;
        this.dbUsername = dbUsername;
        this.dbPassword = dbPassword;

        insertIntoDatabase();
    }

    public void setJdbcDriverClassName(String jdbcDriverClassName) {
        this.jdbcDriverClassName = jdbcDriverClassName;
    }

    public void setJdbcUrl(String jdbcUrl) {
        this.jdbcUrl = jdbcUrl;
    }

    public void setDbUsername(String dbUsername) {
        this.dbUsername = dbUsername;
    }

    public void setDbPassword(String dbPassword) {
        this.dbPassword = dbPassword;
    }

    public void insertIntoDatabase() {

        JDBCTestUtil.connectToDatabase(jdbcDriverClassName, jdbcUrl, dbUsername, dbPassword);

        JDBCTestUtil.executeDatabaseUpdate("DELETE FROM " + Discount.class.getSimpleName().toUpperCase());
        JDBCTestUtil.executeDatabaseUpdate("DELETE FROM " + Product.class.getSimpleName().toUpperCase());

        Product product = new Product(
                TestData.Product1.SKU,
                TestData.Product1.PRODUCT_NAME,
                TestData.Product1.UNIT_PRICE
        );
        insertProductIntoDatabase(product);
        insertDiscountIntoDatabase(new Discount(
                TestData.Discount1.UUID,
                TestData.Discount1.AMOUNT,
                TestData.Discount1.SPECIAL_PRICE,
                TestData.Discount1.SKU
        ));

        JDBCTestUtil.closeDatabase();
    }

    private void insertProductIntoDatabase(Product product) {
        String sqlStatement = "INSERT INTO " + Product.class.getSimpleName().toUpperCase() +
                "(SKU, PRODUCT_NAME, UNIT_PRICE) " +
                "VALUES('" + product.getSku() + "', '"
                + product.getProductName() + "', "
                + product.getUnitPrice() + " );";
        JDBCTestUtil.executeDatabaseUpdate(sqlStatement);
    }

    private void insertDiscountIntoDatabase(Discount discount) {
        String sqlStatement = "INSERT INTO " + Discount.class.getSimpleName().toUpperCase() +
                "(UUID, AMOUNT, SPECIAL_PRICE, SKU) " +
                "VALUES('" + discount.getUuid() + "', "
                + discount.getAmount() + ", "
                + discount.getSpecialPrice() + ", '"
                + "A' );";
        JDBCTestUtil.executeDatabaseUpdate(sqlStatement);
    }

}