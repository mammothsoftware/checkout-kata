package com.pawsey.checkoutkata.service;

import com.pawsey.checkoutkata.model.Discount;
import com.pawsey.checkoutkata.model.Product;
import com.pawsey.checkoutkata.model.SaleItem;
import com.pawsey.checkoutkata.repository.DiscountRepository;
import com.pawsey.checkoutkata.repository.ProductRepository;
import com.pawsey.checkoutkata.testutils.TestData;
import javassist.NotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyChar;
import static org.mockito.Mockito.when;

public class SaleServiceTest {

    private ProductRepository mockProductRepository;
    private DiscountRepository mockDiscountRepository;

    @Autowired
    private SaleService sut;

    private SaleItem[] requestSkuList = new SaleItem[3];

    @Before
    public void setUp() {

        Product mockProduct = new Product(
                TestData.Product1.SKU,
                TestData.Product1.PRODUCT_NAME,
                TestData.Product1.UNIT_PRICE
        );
        Discount mockDiscount = new Discount(
                TestData.Discount1.UUID,
                TestData.Discount1.AMOUNT,
                TestData.Discount1.SPECIAL_PRICE,
                TestData.Discount1.SKU
        );

        for(int i = 0; i < 3; i++) {
            SaleItem saleItem = new SaleItem(TestData.Product1.SKU);
            requestSkuList[i] = saleItem;
        }


        List<Discount> discountList = new ArrayList<>();
        discountList.add(mockDiscount);

        mockProductRepository = Mockito.mock(ProductRepository.class);
        mockDiscountRepository = Mockito.mock(DiscountRepository.class);

        when(mockProductRepository.findBySku(anyChar())).thenReturn(mockProduct);
        when(mockDiscountRepository.findBySku(anyChar())).thenReturn(discountList);

        sut = new SaleServiceImpl(mockProductRepository, mockDiscountRepository);
    }

    @Test
    public void testMakeSaleForMatchingAmount() throws NotFoundException {
        Float responseValue = sut.makeSale(requestSkuList);

        assertNotNull(responseValue);
        assertEquals(TestData.Discount1.SPECIAL_PRICE, responseValue, 1);
    }

    @Test
    public void testMakeSaleForGreaterAmount() throws NotFoundException {

        requestSkuList = new SaleItem[5];

        for(int i = 0; i<=4; i++) {
            requestSkuList[i] = new SaleItem(TestData.Product1.SKU);
        }

        float expectedValue = TestData.Discount1.SPECIAL_PRICE + (TestData.Product1.UNIT_PRICE * 2);

        Float responseValue = sut.makeSale(requestSkuList);

        assertNotNull(responseValue);
        assertEquals(expectedValue, responseValue, 1);
    }
}