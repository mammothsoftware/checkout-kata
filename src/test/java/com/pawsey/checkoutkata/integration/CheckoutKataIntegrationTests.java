package com.pawsey.checkoutkata.integration;

import com.pawsey.checkoutkata.testutils.DatabaseTestUtil;
import com.pawsey.checkoutkata.testutils.TestData;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;

@Ignore
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class CheckoutKataIntegrationTests {

    @LocalServerPort
    private int PORT;
    private static final String URL = "http://localhost:";
    private static final String ENDPOINT = "/sale";
    private String FULL_URL;

    protected DatabaseTestUtil databaseTestUtil = new DatabaseTestUtil();

    private TestRestTemplate testRestTemplate = new TestRestTemplate();

    @Before
    public void setUp() {
        connectToDatabase();
        FULL_URL = URL + PORT + ENDPOINT;
    }

    @Test
    public void postProducts_ForTransactionAmount_WithSingleItem_NoDiscount() {
        String jsonString =
                "{" +
                    "\"skuList\":[" +
                        "{" +
                            "\"sku\": \"A\"" +
                        "}" +
                    "]" +
                "}";
        ResponseEntity<String> response = testRestTemplate.postForEntity(FULL_URL, jsonString, String.class);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertNotNull(response);
        assertTrue(response.getBody().contains(String.valueOf(TestData.Product1.UNIT_PRICE)));
    }

    @Test
    public void postProducts_ForTransactionAmount_WithMultipleItems_NoDiscount() {
        String jsonString =
                "{" +
                    "\"skuList\":[" +
                        "{" +
                            "\"sku\": \"A\"" +
                        "}" +
                        "{" +
                            "\"sku\": \"A\"" +
                        "}" +
                    "]" +
                "}";
        ResponseEntity<String> response = testRestTemplate.postForEntity(FULL_URL, jsonString, String.class);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertNotNull(response);
        assertTrue(response.getBody().contains(String.valueOf(TestData.Product1.UNIT_PRICE * 2)));
    }

    @Test
    public void postProducts_ForTransactionAmount_WithMultipleItems_ForDiscount() {
        String jsonString =
                "{" +
                    "\"skuList\":[" +
                        "{" +
                            "\"sku\": \"A\"" +
                        "}" +
                        "{" +
                            "\"sku\": \"A\"" +
                        "}" +
                        "{" +
                            "\"sku\": \"A\"" +
                        "}" +
                    "]" +
                "}";
        ResponseEntity<String> response = testRestTemplate.postForEntity(FULL_URL, jsonString, String.class);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertNotNull(response);
        assertTrue(response.getBody().contains(String.valueOf(TestData.Discount1.SPECIAL_PRICE)));
    }

    private void connectToDatabase() {
        databaseTestUtil.setJdbcDriverClassName("org.postgresql.Driver");
        databaseTestUtil.setJdbcUrl("jdbc:postgresql://localhost:5432/checkout");
        databaseTestUtil.setDbUsername("checkout");
        databaseTestUtil.setDbPassword("");
    }
}
