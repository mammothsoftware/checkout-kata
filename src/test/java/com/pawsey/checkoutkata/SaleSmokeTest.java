package com.pawsey.checkoutkata;

import com.pawsey.checkoutkata.controller.SaleController;
import com.pawsey.checkoutkata.model.Product;
import com.pawsey.checkoutkata.repository.DiscountRepository;
import com.pawsey.checkoutkata.repository.ProductRepository;
import com.pawsey.checkoutkata.service.SaleService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations="classpath:test.properties")
@ActiveProfiles("test")
public class SaleSmokeTest {

    @Autowired
    private SaleController controller;

    @Autowired
    private SaleService service;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private DiscountRepository discountRepository;

    @Test
    public void contexLoads() {
        assertNotNull("SaleController is null", controller);
        assertNotNull("SaleService is null", service);
        assertNotNull("ProductRepository is null", productRepository);
        assertNotNull("DiscountRepository is null", discountRepository);
    }
}