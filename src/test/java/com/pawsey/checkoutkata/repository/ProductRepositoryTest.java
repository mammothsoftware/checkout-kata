package com.pawsey.checkoutkata.repository;

import com.pawsey.checkoutkata.model.Product;
import com.pawsey.checkoutkata.testutils.DatabaseTestUtil;
import com.pawsey.checkoutkata.testutils.TestData;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@TestPropertySource(locations = "classpath:test.properties")
@ActiveProfiles("test-integration")
public class ProductRepositoryTest {

    protected DatabaseTestUtil databaseTestUtil = new DatabaseTestUtil();

    @Autowired
    private ProductRepository sut;


    @Before
    public void setUp() {
        if(sut == null) {
            fail("Autowiring of ProductRepository failed.");
        }
    }

    @Test
    public void testRead() {
        Product readEntity = sut.findBySku(TestData.Product1.SKU);

        assertNotNull(readEntity);
        assertEquals(TestData.Product1.SKU, readEntity.getSku());
        assertEquals(TestData.Product1.PRODUCT_NAME, readEntity.getProductName());
        assertEquals(TestData.Product1.UNIT_PRICE, readEntity.getUnitPrice(), 1);
    }
}