# checkout_kata

A simple supermarket checkout application, as a Spring-Boot REST API, including PostgreSQL DDL.

## Database
DDL for the database is included in the 'sql' directory within the root. Also included is a dummy data insert file, which will create a product called 'Grapes', with a unit_price of �1, and a discount of �2 for 3.

## Building the application
This application uses Gradle as it's build tool, and creates a 'fat-JAR', using an inbuild Tomcat container to simplify deployment. Documentation for getting started with Gradle can be found [here](https://spring.io/guides/gs/gradle/). 

To build the project, use the command `./gradlew clean build`

## Deployment
Once you've built the application and deployed the PostgreSQL database for it, From the project root run the command `java -jar build/libs/checkout-kata-0.0.1-SNAPSHOT.jar`

## Usage
This is a Rest API which will take a HTTP POST at the endpoint `/sale` containing an array of JSON objects, each of which must have a `sku` field with a single character value corresponding to the `sku` identifier of a Product in the checkout-kata database. With this, it will find matching products and their discounts, and use them to calculate a total cost for the sale.
In doing this, it can calculate single unit prices and multiples of them. If corresponding discounts are found, it will always attempt to use the discounted price where possible to make this calculation; If multiple discounts are possible, it will use them, beginning with the discount for the greatest number of sale-items applicable, and any remaining sale items will be added as single unit-prices for their products.

Below is a sample request, to be made to `http://localhost:8080/sale`:

`[{"sku":"A"},{"sku":"A"},{"sku":"A"}]`

To modify the total cost returned, change the number of objects in the array, e.g.

`[{"sku":"A"},{"sku":"A"}]`